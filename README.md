# NgxBulma
Lightweight UI components for Angular 2+ based on [Bulma](https://bulma.io/)

![Image of Yaktocat](https://gitlab.com/lcatano/ngx-bulma-ui/raw/develop/docs/images/ngx_bulma_ui.png)

**This's a development project, still not yet ready to use.**

**MIT Licence**
